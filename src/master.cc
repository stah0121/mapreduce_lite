#include <unistd.h>
#include "master.h"

using grpc::Channel;
using grpc::ClientAsyncResponseReader;
using grpc::ClientContext;
using grpc::CompletionQueue;
using grpc::Status;
using masterworker::MasterWorker;
using masterworker::WorkReq;
using masterworker::WorkReply;

/******************************************************************************
 * WORKER CLIENT
 * ***************************************************************************/

/* constructor */
WorkerClient::WorkerClient(std::shared_ptr<grpc::Channel> channel, std::string& ipaddr) :
	stub(MasterWorker::NewStub(channel)), ip_port(ipaddr)
{
	state = IDLE;
}

/* RPC interface */
void WorkerClient::MapReduceWork(masterworker::WorkReq query)
{
	ClientContext	context;

	AsyncClientCall *call = new AsyncClientCall;
	inprog_tag = (void *)call;

	DEBUG2("CREATED TAG:%p\n", call);

	call->rsp_reader = stub->AsyncMapReduceWork(&call->context, query, &cq);
	call->rsp_reader->Finish(&call->reply, &call->status, (void *)call);
}

/* RPC response handler */
void WorkerClient::AsyncComplete(void *c, void *m, int client_num)
{
	void			*got_tag = NULL;
	bool			ok = false;
	Master			*mast = static_cast<Master*>(m);
	WorkerClient	*client = static_cast<WorkerClient*>(c);

	cq.Next(&got_tag, &ok);
	if (ok == false) {
		fprintf(stderr, "cq->Next false!\n");
		return;
	}

	AsyncClientCall *call = static_cast<AsyncClientCall *>(got_tag);

	DEBUG("GOT_TAG:%p ok:%s type:%s\n",
			got_tag, ok == true ? "TRUE" : "FALSE", call->reply.type().c_str());

	/* if a job failed, reset our internals and reschedule */
	if (!call->status.ok()) {
		DEBUG2("AsyncClient RPC [FAILED] (tag:%p) rescheduling!\n", got_tag);

		/* reschedule job */
		mast->update_task(call->reply.type(), (void *)call, INIT);

		/* delete failed tag */
		delete call;

		/* fault client and return */
		mtx.lock();
		state = FAULTY;
		mtx.unlock();
		return;
	}
	//DEBUG("ReplyType: ");
	//std::cout << call->reply.type() << std::endl;

	if (call->reply.type() == "MAP") {
		for (const std::basic_string<char>& file : call->reply.interm_files())
			mast->save_intermediate(file.c_str());

	} else if (call->reply.type() == "REDUCE") {
		DEBUG("reduce task completed r_value:%d out_file:%s\n",
				call->reply.r_value(), call->reply.output_file().c_str());

	} else {
		fprintf(stderr, "invalid message type in asynccomplete! (%s)\n",
				call->reply.type().c_str());
	}

	/* mark job as done */
	mast->update_task(call->reply.type(), (void *)call, DONE);

	delete call;

	mtx.lock();
	state = IDLE;
	mtx.unlock();

	DEBUG2("client:%d DONE, reset state to IDLE\n", client_num);
}

/******************************************************************************
 * MASTER
 * ***************************************************************************/

/*
 * CS6210_TASK: This is all the information your master will get from the framework.
 * You can populate your other class data members here if you want
 */
Master::Master(const MapReduceSpec& mr_spec, const std::vector<FileShard>& file_shards)
{
	std::vector<std::string>	workers;

	mr_config.num_workers	= mr_spec.num_workers;
	mr_config.num_out_files = mr_spec.num_out_files;
	mr_config.map_kb 		= mr_spec.map_kb;
	mr_config.map_bytes 	= mr_spec.map_bytes;
	mr_config.userid 		= mr_spec.userid;
	mr_config.output_dir 	= mr_spec.output_dir;
	mr_config.worker_list 	= mr_spec.worker_list;
	mr_config.in_files 		= mr_spec.in_files;

	/* obtain copy of the shards */
	shards = file_shards;

	workers = mr_spec.worker_list;
	for (int i = 0; i < (int)workers.size(); i++) {
		/* create and push new client to our vector */
		clients.push_back(new WorkerClient(grpc::CreateChannel(workers[i].c_str(),
						grpc::InsecureChannelCredentials()), workers[i]));
		DEBUG("created and added client (%s)\n", workers[i].c_str());
	}

	/* save off shards so we can manipulate the MapTasks work queue */
	for (int i = 0; i < (int)shards.size(); i++) {
		map_tasks.push_back(shards[i]);
		map_tasks[i].state = INIT;
	}
	DEBUG("saved (%d) shards for future processing\n", (int)shards.size());
}

/*
 * CS6210_TASK: Here you go. once this function is called you will complete whole map
 * reduce task and return true if succeeded
 */
bool Master::run()
{
	/*
	 * -> Send FileShard to worker and he will create R intermediate files for each
	 * -> Save all the locations of the intermediate file locations in the master
	 */
	RunMapJob();

	/*
	 * -> Group intermediate files into predetermined partitions as a ReduceTask
	 * -> Send interm_files to worker to be 'Reduced' into a single output file foreach {1..R}
	 */
	RunReduceJob();

	/* cleanup */
	for (int i = 0; i < (int)reduce_tasks.size(); i++)
		delete reduce_tasks[i];
	for (int i = 0; i < (int)clients.size(); i++)
		delete clients[i];

	return true;
}

void Master::save_intermediate(const char *file)
{
	DEBUG("saving intermediate file:%s to master\n", file);

	mtx.lock();
	m_interm_files.push_back(std::string(file));
	mtx.unlock();
}

FileShard * Master::get_next_map_task()
{
	FileShard		cur_shard;

	for (int i = 0; i < (int)map_tasks.size(); i++) {
		mtx.lock();

		cur_shard = map_tasks[i];
		if (cur_shard.state == INIT) {
			DEBUG("found shard (%d) with INIT state!\n", i);

			map_tasks[i].inprog_tag = NULL;
			map_tasks[i].state = INPROG;
			mtx.unlock();
			return (&map_tasks[i]);
		}
		mtx.unlock();
	}

	return (NULL);
}

ReduceTask * Master::get_next_reduce_task()
{
	ReduceTask		*ptr = NULL;

	for (int i = 0; i < (int)reduce_tasks.size(); i++) {
		mtx.lock();

		ptr = reduce_tasks[i];
		if (ptr->state == INIT) {
			DEBUG2("found ReduceTask:%p (%d) with INIT state!\n",
					ptr, ptr->partition_num);

			ptr->inprog_tag = NULL;
			ptr->state = INPROG;
			mtx.unlock();
			return (ptr);
		}
		mtx.unlock();
	}

	return (NULL);
}

/* if a reduce job fails, remove the output-file associated with the failed job */
void cleanup_reduce_file(std::string& client, std::string& out_dir, int r_value)
{
	int key = atoi(client.substr(client.find_first_of(":")+1).c_str());
	char buf[80];

	sprintf(buf, "%s/reduced_%d_%d.txt", out_dir.c_str(), key, r_value);
	if(remove(buf) != 0)
		DEBUG2("Error deleting file:%s\n", buf);
}

void Master::update_task(const std::string& type, void *tag, int state)
{
	int cleanup = 0;

	DEBUG2("update_task type:%s tag:%p to_state:%d\n",
			type.c_str(), tag, state);

	/*
	 * Loop on all map tasks looking for our tag, and update the job state
	 */
	if (type == "MAP") {
		for (int i = 0; i < (int)map_tasks.size(); i++) {
			mtx.lock();
			if (map_tasks[i].inprog_tag == tag) {
				DEBUG2("map_task state updated! tag:%p\n", tag);
				map_tasks[i].state = state;
				if (state == DONE)
					map_tasks[i].inprog_tag = NULL;
				mtx.unlock();
				return;
			}
			mtx.unlock();
		}
		DEBUG2("[ERROR] task not found for state update!\n");
		return;
	}

	/*
	 * Loop on all reduce tasks looking for our tag, and update the job state
	 */
	if (type == "REDUCE") {
		for (int i = 0; i < (int)reduce_tasks.size(); i++) {
			mtx.lock();
			if (reduce_tasks[i]->inprog_tag == tag) {
				DEBUG2("reduce_task (idx:%d) tag:%p STATE (%d->%d)\n", i, tag,
						reduce_tasks[i]->state, state);
				reduce_tasks[i]->state = state;
				if (state == DONE)
					reduce_tasks[i]->inprog_tag = NULL;
				mtx.unlock();
				return;
			}
			mtx.unlock();
		}
		DEBUG2("[ERROR] task not found for state update!\n");
		return;
	}

	/*
	 * no TaskType indicator (likely) means an RPC error occurred
	 * cleanup reduce output file(s) if needed -- interm files can remain
	 */
	DEBUG2("TaskType not found! searching all tasks [tag:%p]\n", tag);
	for (int i = 0; i < (int)map_tasks.size(); i++) {
		mtx.lock();
		if (map_tasks[i].inprog_tag == tag) {
			DEBUG2("map_task state updated! tag:%p\n", tag);
			map_tasks[i].state = state;
			if (state == DONE)
				map_tasks[i].inprog_tag = NULL;
			mtx.unlock();
			return;
		}
		mtx.unlock();
	}
	for (int i = 0; i < (int)reduce_tasks.size(); i++) {
		mtx.lock();
		if (reduce_tasks[i]->inprog_tag == tag) {
			DEBUG2("reduce_task state updated! tag:%p \n", tag);
			if (reduce_tasks[i]->state == INPROG && state == INIT)
				cleanup = 1;
			reduce_tasks[i]->state = state;
			if (state == DONE)
				reduce_tasks[i]->inprog_tag = NULL;
			mtx.unlock();

			/* cleanup stragler output file */
			if (cleanup) {
				cleanup_reduce_file(reduce_tasks[i]->client,
						mr_config.output_dir,
						reduce_tasks[i]->partition_num);
			}

			return;
		}
		mtx.unlock();
	}

	DEBUG2("[ERROR] task not found for state update!\n");
}

/* returns 1 on MR-DONE scenario, returns 0 if not done */
int Master::mapreduce_complete(const std::string& type)
{
	static int reduce_stats = 200;
	int init = 0, inprog = 0, done = 0;

	if (type == "MAP") {
		for (int i = 0; i < (int)map_tasks.size(); i++) {
			if (map_tasks[i].state != DONE) {
				return (0);
			}
		}
	} else if (type == "REDUCE") {
		for (int i = 0; i < (int)reduce_tasks.size(); i++) {
			if (reduce_tasks[i]->state == INIT) {
				init++;
			} else if (reduce_tasks[i]->state == INPROG) {
				inprog++;
				if (reduce_stats)
					DEBUG("ReduceTask INPROG:%p\n", reduce_tasks[i]->inprog_tag);
			} else if (reduce_tasks[i]->state == DONE) {
				done++;
			}
		}
		if (reduce_stats) {
			DEBUG2("[REDUCE JOBS] init:%d inprog:%d done:%d\n", init, inprog, done);
			reduce_stats--;
		}
		if ((int)reduce_tasks.size() != done)
			return (0);

	} else {
		DEBUG2("[INVALID JOB TYPE] (%s)\n", type.c_str());
		return (0);
	}

	/* I guess we're done [all jobs marked DONE and all clients IDLE] */

	return (1);
}

void Master::RunMapJob()
{
	std::thread					*ptr = NULL;
	FileShard					*cur_shard = NULL;
	std::string					*msg_arg = NULL;
	ShardEntry					*cur_entry;
	char						buf[80];
	masterworker::WorkReq		work_req;

	while (1) {

map_start:

		/*
		 * get next map-task and continue to loop until we find a free
		 * worker to serve the task. once that happens the task will be
		 * updated and eventaully the exit condition will be satisfied
		 */
		if (cur_shard == NULL)
			cur_shard = get_next_map_task();

		/*
		 * check exit conditions
		 * -> all clients in IDLE state (no work being processed)
		 * -> check the states of all the clients looking for completion, else go back
		 */
		if (cur_shard == NULL) {
			for (int i = 0; i < (int)clients.size(); i++) {
				/* someone still working */
				if (clients[i]->state == BUSY) {
					/* wait (5ms) for workers to finish */
					usleep((5 * 1000));
					goto map_start;
				}
			}
			/*
			 * here we dont know about any existing map-tasks needing service and no
			 * clients are working. Check if we are done or need to reschedule some tasks
			 */
			if (mapreduce_complete("MAP")) {
				break;
			} else {
				/* keep waiting */
				usleep((5 * 1000));
				goto map_start;
			}
		}

		/* if MapTasks still exist, try to send out work */
		for (int i = 0; i < (int)clients.size(); i++) {
			/*
			 * allow our for loop to distribute many shards worth of work if workers
			 * are available, and if we run out while in this loop, go to the top
			 * and wait for our exit conditions
			 */
			if (cur_shard == NULL) {
				cur_shard = get_next_map_task();
				if (cur_shard == NULL)
					goto map_start;
			}

			/* check if client is ready for work */
			clients[i]->mtx.lock();
			if (clients[i]->state != IDLE) {
				clients[i]->mtx.unlock();
				continue;
			}
			/* indicate this worker is busy and clear prev tag */
			clients[i]->state = BUSY;
			clients[i]->inprog_tag = NULL;
			clients[i]->mtx.unlock();

			/* associate client number with maptask */
			cur_shard->client_num = i;

			/* clear our message details if there are any */
			work_req.clear_interm_files();
			work_req.clear_shard_entry();
			work_req.clear_type();
			work_req.clear_userid();
			work_req.clear_r_value();

			/* loop on all ShardEntry objects appending to RPC message */
			for (int j = 0; j < (int)cur_shard->entries.size(); j++) {
				cur_entry = &cur_shard->entries[j];

				/* create new grpc arg to hold our ShardEntry */
				msg_arg = work_req.add_shard_entry();
				if (!msg_arg) {
					printf("[ERR] msg_arg failed to create!\n");
					break;
				}
				/*
				 * format and assign a null terminated string representing
				 * this ShardEntry
				 */
				memset(buf, 0, sizeof(buf));
				sprintf(buf, "%s:%d:%d", cur_entry->in_file.c_str(),
						cur_entry->start, cur_entry->end);
				msg_arg->assign(buf);
			}

			/* set environment variables */
			work_req.set_type("MAP");
			work_req.set_r_value(mr_config.num_out_files);
			work_req.set_userid(mr_config.userid);

			DEBUG("RPC call (%s): type:MAP R:%d user:%s numShardEntries:%d client:%d\n",
					clients[i]->ip_port.c_str(), mr_config.num_out_files,
					mr_config.userid.c_str(), (int)cur_shard->entries.size(), i);

			/* make RPC call finally */
			clients[i]->MapReduceWork(work_req);

			/* associate client tag with job */
			cur_shard->inprog_tag = clients[i]->inprog_tag;

			/* spawn thread to wait for response */
			ptr = new std::thread(std::bind(&WorkerClient::AsyncComplete, clients[i],
						static_cast<void*>(clients[i]), static_cast<void*>(this), i));
			ptr->detach();

			/* reset cur_shard so we will attempt to get another */
			cur_shard = NULL;

			DEBUG("started MapTask thread for client:%d\n", i);
		}

		/* sleep for 10ms to let one of the workers finish hopefully */
		usleep((10 * 1000));

	}// end-while map_tasks
}

/* helper routine to get partition number from interm file */
int get_interm_part_num(std::string& file)
{
	int pos = file.find_last_of("_")+1;
	std::string tmp = file.substr(pos, file.find_last_of("."));
	return (atoi(tmp.c_str()));
}

void Master::RunReduceJob()
{
	std::string					*interm = NULL;
	std::thread					*ptr = NULL;
	FileShard					cur_shard;
	std::string					*msg_arg = NULL;
	ShardEntry					*cur_entry;
	char						buf[80];
	ReduceTask 					*rtask = NULL;
	masterworker::WorkReq		work_req;
	std::string 				tmp;
	int 						pnum, pos;

	/* setup reduce task list */
	for (int i = 1; i <= mr_config.num_out_files; i++) {
		rtask = new ReduceTask();
		rtask->partition_num = i;
		rtask->userid = mr_config.userid;
		rtask->state = INIT;
		DEBUG("intialized reduce-task number:%d\n", i);
		reduce_tasks.push_back(rtask);

		/* force later code to do a lookup */
		rtask = NULL;
	}

	/* attach R-values with intermediate files */
	DEBUG2("Adding (%d) interm_files to all partition structures\n",
			(int)m_interm_files.size());

	/* group interm files by their R value */
	for (int i = 0; i < (int)m_interm_files.size(); i++) {
		pos = m_interm_files[i].find_last_of("_")+1;
		tmp = m_interm_files[i].substr(pos, m_interm_files[i].find_last_of("."));
		pnum = atoi(tmp.c_str());
		if (pnum < 1 || pnum > mr_config.num_out_files) {
			fprintf(stderr, "pnum:%d invalid!\n", pnum);
			return;
		}
		rt_add_interm(pnum, m_interm_files[i]);
	}

	while (1) {

		/* label this location */
reduce_start:

		/* check for available reduce tasks */
		if (rtask == NULL)
			rtask = get_next_reduce_task();

		/*
		 * check exit conditions
		 * -> if ReduceTasks empty then no work distribution code (below) should run
		 * -> check the states of all the clients looking for completion, else go back
		 */
		if (rtask == NULL) {
			for (int i = 0; i < (int)clients.size(); i++) {

				/* someone is still working */
				if (clients[i]->state == BUSY) {
					/* wait (5ms) for workers to finish */
					usleep((5 * 1000));
					goto reduce_start;
				}
			}
			/*
			 * here we dont know about any existing reduce-tasks needing service and no
			 * clients are working. Check if we are done or need to reschedule some tasks
			 */
			if (mapreduce_complete("REDUCE")) {
				break;
			} else {
				/* keep waiting */
				usleep((5 * 1000));
				goto reduce_start;
			}
			break;
		}

		for (int i = 0; i < (int)clients.size(); i++) {

			if (rtask == NULL) {
				rtask = get_next_reduce_task();
				if (rtask == NULL)
					goto reduce_start;
			}

			/* check if client is ready for work */
			clients[i]->mtx.lock();
			if (clients[i]->state != IDLE) {
				clients[i]->mtx.unlock();
				continue;
			}
			/* indicate this worker is busy */
			clients[i]->state = BUSY;
			clients[i]->mtx.unlock();

			/* clear our message details if there are any */
			work_req.clear_interm_files();
			work_req.clear_shard_entry();
			work_req.clear_type();
			work_req.clear_userid();
			work_req.clear_r_value();

			/* set environment variables */
			work_req.set_type("REDUCE");
			work_req.set_r_value(rtask->partition_num);
			work_req.set_userid(mr_config.userid);
			work_req.set_output_dir(mr_config.output_dir);

			/* get together partition based intermediate file list */
			for (int i = 0; i < (int)m_interm_files.size(); i++) {
				if (rtask->partition_num != get_interm_part_num(m_interm_files[i]))
					continue;

				interm = work_req.add_interm_files();
				if (!interm) {
					fprintf(stderr, "add_interm_files failed!\n");
					break;
				}
				DEBUG("Adding file:%s to RPC request message\n",
						m_interm_files[i].c_str());
				interm->assign(m_interm_files[i]);
			}
			DEBUG2("send reduce-task:%p RPC for R:%d (client:%s)\n",
					rtask, rtask->partition_num, clients[i]->ip_port.c_str());

			/* make RPC call finally */
			clients[i]->MapReduceWork(work_req);

			/* associate client tag with job */
			rtask->inprog_tag = clients[i]->inprog_tag;
			rtask->client = clients[i]->ip_port;

			/* spawn thread to wait for a response */
			ptr = new std::thread(std::bind(&WorkerClient::AsyncComplete, clients[i],
						static_cast<void*>(clients[i]), static_cast<void*>(this), i));
			ptr->detach();

			/* reset rtask so we will attempt to get another */
			rtask = NULL;

			DEBUG("started ReduceTask thread for client:%d\n", i);
		}
	}// end-while-reducejobs
}
