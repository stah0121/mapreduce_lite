#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG(_fmt, ...)

#define DEBUG2(_fmt, ...)	{				\
	printf("%s:%d ", __FILE__, __LINE__); 	\
	printf((_fmt), ##__VA_ARGS__);			\
}

/*
 * CS6210_TASK Implement this data structureas per your implementation.
 * You will need this when your worker is running the map task
 */
struct BaseMapperInternal {

		/* DON'T change this function's signature */
		BaseMapperInternal();

		/* DON'T change this function's signature */
		void emit(const std::string& key, const std::string& val);

		/*
		 * NOW you can add below, data members and member functions as per
		 * the need of your implementation
		 */
		void init_output(int key, int task_num, std::string& user, int r_in);
		const std::vector<std::string>& finalize();
		void flush_buffers();

		int							R;
		std::vector<std::string>	out_files;
		std::string					userid;
		std::vector<FILE *>			interm_fp;
		std::vector<std::string>	interm_buf;
};

/* CS6210_TASK Implement this function */
inline BaseMapperInternal::BaseMapperInternal()
{
	/* didn't end up needing this */
}

inline void BaseMapperInternal::init_output(int key, int task_num, std::string& user, int r_in)
{
	FILE		*fp = NULL;
	char		buf[80];

	/* set internal variables */
	R = r_in;
	userid = user;

	interm_fp.clear();
	interm_buf.clear();
	out_files.clear();
	/*
	 * create intermediate files for output
	 * FORMAT: <userid>_interm<task#>_<key>_<1..R>.txt
	 */
	for (int i = 1; i <= R; i++) {
		memset(buf, 0, sizeof (buf));
		sprintf(buf, "%s_interm%d_%d_%d.txt", userid.c_str(), task_num, key, i);
		out_files.push_back(std::string(buf));
	}
}

static inline int get_interm_part_num(std::string& file)
{
	int pos = file.find_last_of("_")+1;
	std::string tmp = file.substr(pos, file.find_last_of("."));
	return (atoi(tmp.c_str()));
}

/*
 * every 1000 or so buffers, open file(s) and flush to disk
 */
#define INTERM_BUF_LIMIT	1000
inline void BaseMapperInternal::flush_buffers()
{
	int					r_val;
	unsigned long int	key_hash;
	std::string			new_entry;
	std::string			tmp;
	std::string			key;
	std::string 		partition;
	int					pnum;
	FILE				*fp = NULL;
	FILE				*fp_out[64];

	memset(fp_out, 0, sizeof (fp_out));
	for (int i = 0; i < (int)out_files.size(); i++) {

		pnum = get_interm_part_num(out_files[i]);

		DEBUG("opening handle to file:%s pnum:%d\n",
				out_files[i].c_str(), pnum);

		fp = fopen(out_files[i].c_str(), "a");
		if (!fp) {
			DEBUG2("fopen failed errno:%s\n", strerror(errno));
			break;
		}
		fp_out[pnum] = fp;
	}

	DEBUG("flushing (%d) buffers to disk\n", (int)interm_buf.size());

	while (!interm_buf.empty()) {
		tmp = interm_buf.back();
		interm_buf.pop_back();

		key = tmp.substr(0, tmp.find_first_of("|"));

		/* get unsigned hex representation of the key for partitioning */
		key_hash = strtoul(key.c_str(), (char **)NULL, 16);

		/* identify our partition */
		r_val = (int)(key_hash % R);
		r_val++;

		if (r_val > 0 && r_val < 64)
			fprintf(fp_out[r_val], "%s", tmp.c_str());
		else
			DEBUG2("r_val:%d OUT OF RANGE\n", r_val);
	}

	if (!interm_buf.empty())
		DEBUG2("[FATAL-ERROR] expected to empty all buffers here!\n");

	for (int i = 0; i < (int)out_files.size(); i++) {
		pnum = get_interm_part_num(out_files[i]);
		fclose(fp_out[pnum]);
	}
}

/* CS6210_TASK Implement this function */
inline void BaseMapperInternal::emit(const std::string& key, const std::string& val)
{
	std::string			new_entry;

	/*
	 * FORMAT for intermediate key-value pairs:
	 * 		key|val'\n'
	 * 		key|val'\n'
	 */

	/* count newline characters but dont allow them to affect our interm/out files */
	new_entry.resize(key.length());
	for (int i = 0; i < key.length(); i++) {
		if (key[i] == '\n')
			new_entry[i] = 'N';
		else
			new_entry[i] = key[i];
	}

	new_entry.append("|");
	new_entry.append(val.c_str());
	new_entry.append("\n");

	interm_buf.push_back(new_entry);

	if (interm_buf.size() >= 1000) {
		flush_buffers();
	}
}

inline const std::vector<std::string>& BaseMapperInternal::finalize()
{
	/* flush any lingering entries */
	if (!interm_buf.empty()) {
		flush_buffers();
	}

	interm_buf.clear();

	return (out_files);
}

/*-----------------------------------------------------------------------------------------------*/

/* represents what the user reduce function expects as arguments */
struct ReduceRecord {
	std::string					key;
	std::vector<std::string>	values;
};

static bool record_sort(ReduceRecord r1, ReduceRecord r2)
{
	int rc = strcasecmp(r1.key.c_str(), r2.key.c_str());

	/* denotes r1 comes before r2 */
	if (rc < 0)
		return (true);

	return (false);
}

/*
 * CS6210_TASK Implement this data structureas per your implementation.
 * You will need this when your worker is running the reduce task
 */
struct BaseReducerInternal {

		/* DON'T change this function's signature */
		BaseReducerInternal();

		/* DON'T change this function's signature */
		void emit(const std::string& key, const std::string& val);

		/*
		 * NOW you can add below, data members and member functions as per the
		 * need of your implementation
		 */
		std::vector<ReduceRecord>& get_records() { return (records); }

		void update_reduce_record(std::string& key, std::string& value);
		void parse_interm_data(FILE *fp);
		void init(int r_value, const char *out_dir, int worker_key,
				std::vector<std::string>& file_list);
		std::string& finalize();

		int							partition;
		std::vector<ReduceRecord>	records;
		std::vector<std::string>	r_interm_files;
		std::string					out_file;
		FILE						*out_fp;
};

/* CS6210_TASK Implement this function */
inline BaseReducerInternal::BaseReducerInternal()
{
	/* didnt end up needing this */
}

inline void BaseReducerInternal::update_reduce_record(std::string& key, std::string& value)
{
	ReduceRecord entry;

	for (int i = 0; i < (int)records.size(); i++) {
		if (strcasecmp(key.c_str(), records[i].key.c_str()) == 0) {
			/* found duplicate key, append value */
			records[i].values.push_back(std::string(value.c_str()));
			return;
		}
	}
	DEBUG("adding new key:%s to ReduceRecords\n", key.c_str());

	entry.key = key;
	entry.values.push_back(std::string(value));

	records.push_back(entry);
}

inline void BaseReducerInternal::parse_interm_data(FILE *fp)
{
	ssize_t			nread = 0;
	size_t			len = 0;
	char			*l = NULL;
	std::string		key, value;
	std::string		line;

	while ((nread = getline(&l, &len, fp)) != -1) {
		/*
		 * each line in the interm_files contains a <key>|<value> pair
		 * separated by the '|' character, and pairs separated by '\n'
		 */
		line = l;

		key = line.substr(0, line.find_first_of("|"));
		value = line.substr(line.find_first_of("|")+1);

		update_reduce_record(key, value);
	}
}

inline void BaseReducerInternal::init(int r_value, const char *out_dir, int worker_key,
		std::vector<std::string>& file_list)
{
	char		buf[80];
	FILE		*fp = NULL;

	/* initialize our partition number */
	partition = r_value;

	/* initialize our output file */
	sprintf(buf, "%s/reduced_%d_%d.txt", out_dir, worker_key, r_value);
	out_file = buf;
	DEBUG2("reducer output file set:%s\n", out_file.c_str());

	out_fp = fopen(out_file.c_str(), "w");
	if (!out_fp) {
		fprintf(stderr, "fopen failed! (%s) rc:%s\n", buf, strerror(errno));
		return;
	}

	/*
	 * open all intermediate files and scan each key/value entry eventually saving off
	 * a list of unique key entries and corresponding list of values seen with that key
	 */
	DEBUG("interm_file_list_cnt:%d\n", (int)file_list.size());
	for (int i = 0; i < (int)file_list.size(); i++) {
		DEBUG("opening %s interm_file for ReduceTask\n", file_list[i].c_str());

		fp = NULL;
		fp = fopen(file_list[i].c_str(), "r");
		if (!fp) {
			fprintf(stderr, "failed to open:%s rc:%s\n", file_list[i].c_str(), strerror(errno));
			return;
		}
		/* add file to our internal list */
		r_interm_files.push_back(std::string(file_list[i].c_str()));
		/* parse all file data */
		parse_interm_data(fp);
		/* cleanup */
		fclose(fp);
	}

	/* sort our current ReduceRecord vector per our custom sort function (by key) */
	std::sort(records.begin(), records.end(), record_sort);
}

/* CS6210_TASK Implement this function */
inline void BaseReducerInternal::emit(const std::string& key, const std::string& val)
{
	if (out_fp)
		fprintf(out_fp, "%s %s\n", key.c_str(), val.c_str());
	else
		fprintf(stderr, "out_fp NULL! [k:%s v:%s]\n", key.c_str(), val.c_str());
}

inline std::string& BaseReducerInternal::finalize()
{
	if (out_fp)
		fclose(out_fp);
	out_fp = NULL;

	records.clear();
	r_interm_files.clear();

	return (out_file);
}
