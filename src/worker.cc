#include <string.h>
#include "worker.h"

using grpc::Status;
using grpc::Server;
using grpc::ServerAsyncResponseWriter;
using grpc::ServerCompletionQueue;
using grpc::ServerContext;
using grpc::ServerBuilder;
using masterworker::MasterWorker;
using masterworker::WorkReq;
using masterworker::WorkReply;

/******************************************************************************
 * Helper and Entry Point function definitions
 * ***************************************************************************/

/*
 * CS6210_TASK: ip_addr_port is the only information you get when started.  You can populate your
 * other class data members here if you want
 */
Worker::Worker(std::string ip_addr_port)
{
	Init(ip_addr_port);
}

/*
 * CS6210_TASK: Here you go. once this function is called your woker's job is to keep looking for new
 * tasks from Master, complete when given one and again keep looking for the next one. Note that you
 * have the access to BaseMapper's member BaseMapperInternal impl_ and BaseReduer's member
 * BaseReducerInternal impl_ directly, so you can manipulate them however you want when running
 * map/reduce tasks
 */
bool Worker::run() {

	RunServer();

	return true;
}


/******************************************************************************
 * MsgData function definitions
 * ***************************************************************************/

MsgData::MsgData(MasterWorker::AsyncService *srvc, ServerCompletionQueue *cq) :
	service(srvc), cq(cq), respond(&ctx), state(CREATE)
{
	state = CREATE;
}

void MsgData::Register_Request()
{
	if (state != CREATE) {
		printf("MsgData in invalid state:%d\n", state);
		return;
	}

	service->RequestMapReduceWork(&ctx, &req_msg, &respond, cq, cq, this);
}

void MsgData::Process_Request(void *ctx)
{
	std::string	*ptr = NULL;
	Worker		*inst = static_cast<Worker *>(ctx);

	if (state != PROCESS || !inst) {
		printf("MsgData in invalid state:%d inst:%p\n", state, inst);
		return;
	}

	/* process the request */
	if (req_msg.type() == "MAP") {
		/* perform Map task */
		inst->MapTask(req_msg);

		/* setup reply message */
		for (int i = 0; i < (int)inst->w_interm_files.size(); i++) {
			ptr = rsp_msg.add_interm_files();
			if (!ptr) {
				fprintf(stderr, "add_interm_files failed!\n");
				break;
			}
			DEBUG("Adding file:%s to RPC reply message\n",
					inst->w_interm_files[i].c_str());
			ptr->assign(inst->w_interm_files[i]);
		}
		rsp_msg.set_type("MAP");
		rsp_msg.set_r_value(req_msg.r_value());

	} else if (req_msg.type() == "REDUCE") {
		/* perform Reduce task */
		inst->ReduceTask(req_msg);

		/* setup any additional message params */
		rsp_msg.set_type("REDUCE");
		rsp_msg.set_output_file(inst->output_file);
		rsp_msg.set_r_value(req_msg.r_value());

	} else {
		printf("Invalid work type! (%s)\n", req_msg.type().c_str());
	}

	/* indicate completion */
	state = FINISH;
	respond.Finish(rsp_msg, Status::OK, this);
}

/******************************************************************************
 * Worker function definitions
 * ***************************************************************************/

void 
Worker::Init(std::string ip_addr_port)
{
	worker_key = atoi(ip_addr_port.substr(ip_addr_port.find_first_of(":")+1).c_str());
	DEBUG("starting (%s) worker process (key:%d)...\n",
			ip_addr_port.c_str(), worker_key);

	builder.AddListeningPort(ip_addr_port, grpc::InsecureServerCredentials());
	builder.RegisterService(&service);

	cq = builder.AddCompletionQueue();
	server = builder.BuildAndStart();
}

void
Worker::RunServer()
{
	MsgData		*new_ptr = NULL;
	MsgData		*cq_ptr = NULL;
	void		*tag = NULL;
	bool		ok = false;
	int			create_cnt = 0;
	int			finish_cnt = 0;

	DEBUG("Worker server starting...\n");

	/* allocate and register object for first request */
	cq_ptr = new MsgData(&service, cq.get());
	cq_ptr->Register_Request();
	create_cnt++;

	while (true) {

		cq->Next(&tag, &ok);
		if (ok == false) {
			printf("shutting down...\n");
			break;
		}

		DEBUG("got tag:%p ok:%s\n", tag,
				ok == true ? "TRUE" : "FALSE");
		
		cq_ptr = static_cast<MsgData *>(tag);

		if (cq_ptr->state == CREATE) {
			/* Register new obj to replace the one we added to the work queue */
			new_ptr = new MsgData(&service, cq.get());
			new_ptr->Register_Request();
			create_cnt++;

			DEBUG("setting tag:%p to PROCESS\n", cq_ptr);
			cq_ptr->state = PROCESS;
			cq_ptr->Process_Request(this);
			DEBUG("[PROCESS] complete for tag:%p type:%s R:%d\n",
					cq_ptr, cq_ptr->req_msg.type().c_str(), cq_ptr->req_msg.r_value());
		}
		if (cq_ptr->state == FINISH) {
			DEBUG("tag:%p marked as FINISH\n", cq_ptr);
			finish_cnt++;
			//free(cq_ptr);

			/* ensure we have at least 1 request out there */
			if ((create_cnt - finish_cnt) == 0) {
				cq_ptr = new MsgData(&service, cq.get());
				cq_ptr->Register_Request();
				create_cnt++;
				DEBUG2("starting new tag:%p for listening\n", cq_ptr);
			}
		}
	}
}

/******************************************************************************
 * CORE MAP||REDUCE FUNCTIONALITY
 * ***************************************************************************/

extern std::shared_ptr<BaseMapper> get_mapper_from_task_factory(const std::string& user_id);
extern std::shared_ptr<BaseReducer> get_reducer_from_task_factory(const std::string& user_id);

void
Worker::MapTask(masterworker::WorkReq& req_msg)
{
	FILE			*fp;
	std::string		file, s, e;
	int				start, end;
	int				pos1, pos2;
	ssize_t			nread = 0;
	char			*line = NULL;
	size_t			len = 0;
	int				file_pos;
	std::string		user(req_msg.userid().c_str());

	glbl_task_cnt++;
	/*
	 * load file shard info and parse record by record
	 */
	auto mapper = get_mapper_from_task_factory(user.c_str());

	mapper->impl_->init_output(worker_key, glbl_task_cnt, user, req_msg.r_value());
	
	for (const std::string& shard_e : req_msg.shard_entry()) {
		/*
		 * [FORMAT] <filename>:<start>:<end>
		 * get filename first
		 */
		pos1 = shard_e.find_first_of(":");
		file = shard_e.substr(0, pos1);

		/* get starting offset */
		pos2 = shard_e.find_first_of(":", pos1+1);
		s = shard_e.substr(pos1+1, pos2);
		start = atoi(s.c_str());

		/* get ending offset */
		e = shard_e.substr(pos2+1);
		end = atoi(e.c_str());

		DEBUG2("working on input_file:%s range:%d-%d\n",
				file.c_str(), start, end);

		fp = NULL;
		fp = fopen(file.c_str(), "r");
		if (!fp) {
			fprintf(stderr, "failed to open file:%s errno:%s\n",
					file.c_str(), strerror(errno));
			break;
		}

		DEBUG2(" [ShardEntry] range:%d-%d in_file:%s\n",
				start, end, file.c_str());

		file_pos = 0;

		while ((nread = getline(&line, &len, fp)) != -1) {

			if (file_pos >= start && file_pos <= end) {
				//printf("%s", line);
				mapper->map(line);
			}

			if (line) free(line);
			line = NULL;
			len = 0;

			file_pos++;
		}

		fclose(fp);
	} // end-foreach-sEntry

	w_interm_files = mapper->impl_->finalize();

	DEBUG2("worker:%d DONE processing shards. interm_file_cnt:%d\n",
			worker_key, (int)w_interm_files.size());
}

void
Worker::ReduceTask(masterworker::WorkReq& req_msg)
{
	std::string					user(req_msg.userid().c_str());
	std::vector<std::string>	file_list;
	std::vector<ReduceRecord>	inst;

	/* load intermediate file data */
	DEBUG2("worker loading interm_files for ReduceTask..\n");
	for (const std::basic_string<char>& f : req_msg.interm_files()) {
		//printf("%s\n", f.c_str());
		file_list.push_back(std::string(f.c_str()));
	}

	/* load reducer object implementation */
	auto reducer = get_reducer_from_task_factory(user.c_str());

	DEBUG2("calling reducer_init with R:%d\n", (int)req_msg.r_value());
	reducer->impl_->init((int)req_msg.r_value(), req_msg.output_dir().c_str(),
			worker_key, file_list);

	inst = reducer->impl_->get_records();
	DEBUG("reducer found (%d) tasks\n", (int)inst.size());
	for (int i = 0; i < (int)inst.size(); i++) {

		reducer->reduce(inst[i].key, inst[i].values);
	}

	output_file = reducer->impl_->finalize();
}
