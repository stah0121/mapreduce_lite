#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <grpc/support/log.h>
#include "masterworker.grpc.pb.h"

#include <mr_task_factory.h>
#include "mr_tasks.h"

#define DEBUG(_fmt, ...)

#define DEBUG2(_fmt, ...)	{				\
	printf("%s:%d ", __FILE__, __LINE__); 	\
	printf((_fmt), ##__VA_ARGS__);			\
}

enum MsgState { CREATE, PROCESS, FINISH };

class MsgData {
	public:
	MsgData(masterworker::MasterWorker::AsyncService *srvc, grpc::ServerCompletionQueue *cq);

	void Register_Request();
	void Process_Request(void *ctx);

	private:
	friend class Worker;
	masterworker::MasterWorker::AsyncService			*service;
	grpc::ServerCompletionQueue							*cq;
	grpc::ServerContext									ctx;
	masterworker::WorkReq								req_msg;
	masterworker::WorkReply								rsp_msg;
	grpc::ServerAsyncResponseWriter
		<masterworker::WorkReply>						respond;

	MsgState                                state;
};

/*
 * CS6210_TASK: Handle all the task a Worker is supposed to do.  This is a big task
 * for this project, will test your understanding of map reduce
 */
class Worker {

	public:
	/* DON'T change the function signature of this constructor */
	Worker(std::string ip_addr_port);
	/* DON'T change this function's signature */
	bool run();

	void		Init(std::string ip_addr_port);
	void		RunServer();
	void 		MapTask(masterworker::WorkReq& req_msg);
	void 		ReduceTask(masterworker::WorkReq& req_msg);

	private:
	/*
	 * NOW you can add below, data members and member functions as per
	 * the need of your implementation
	 */
	friend class MsgData;
	int												worker_key; // "worker identifier"
	int												glbl_task_cnt;
	std::unique_ptr<grpc::ServerCompletionQueue>    cq;
	masterworker::MasterWorker::AsyncService		service;
	std::unique_ptr<grpc::Server>					server;
	grpc::ServerBuilder								builder;
	std::string										output_file;
	std::vector<std::string>						w_interm_files;
};
