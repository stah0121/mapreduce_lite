#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#define DEBUG(_fmt, ...)

#define DEBUG2(_fmt, ...)	{				\
	printf("%s:%d ", __FILE__, __LINE__); 	\
	printf((_fmt), ##__VA_ARGS__);			\
}

/*
 * CS6210_TASK: Create your data structure here for storing spec from the
 * config file
 */
struct MapReduceSpec {
	int							num_workers;
	int							num_out_files;
	int							map_kb;
	int							map_bytes;

	std::string					userid;
	std::string					output_dir;
	std::vector<std::string>	worker_list;
	std::vector<std::string>	in_files;
};

inline std::vector<std::string>
split(const std::string& s, char delimiter)
{
	std::vector<std::string> tokens;
	std::string token;
	std::istringstream tokenStream(s);
	while (std::getline(tokenStream, token, delimiter))
	{
		tokens.push_back(token);
	}
	return tokens;
}

/*
 * CS6210_TASK: Populate MapReduceSpec data structure with the specification
 * from the config file
 */
inline bool read_mr_spec_from_config_file(const std::string& config_filename,
		MapReduceSpec& mr_spec)
{
	int							REQ_FIELDS = 7;
	std::ifstream				cfg_file(config_filename.c_str(), std::ifstream::in);
	std::string					line;
	std::string					substr;
	std::size_t					pos = 0;
	std::vector<std::string>	value_split;
	/* CONFIG FILE PARAMETERS */
	int							nworkers, noutfiles, map_kb;
	std::string 				userid, output_dir;
	std::vector<std::string>	worker_list, in_files;

	while (std::getline(cfg_file, line)) {
		/* find and load value string */
		pos = line.find("=");
		pos++;
		substr = line.substr(pos);

		/* parse config file keys and take appropriate action */
		if (line.find("n_workers") == 0) {
			REQ_FIELDS--;
			nworkers = atoi(substr.c_str());
			DEBUG("nworkers:%d set!\n", nworkers);
		}
		if (line.find("n_output_files") == 0) {
			REQ_FIELDS--;
			noutfiles = atoi(substr.c_str());
			DEBUG("noutfiles:%d set!\n", noutfiles);
		}
		if (line.find("map_kilobytes") == 0) {
			REQ_FIELDS--;
			map_kb = atoi(substr.c_str());
			DEBUG("map_kb:%d set!\n", map_kb);
		}
		if (line.find("user_id") == 0) {
			REQ_FIELDS--;
			userid = substr;
		}
		if (line.find("output_dir") == 0) {
			REQ_FIELDS--;
			output_dir = substr;
		}
		if (line.find("worker_ipaddr_ports") == 0) {
			REQ_FIELDS--;
			value_split = split(substr, ',');
			for (int i = 0; i < (int)value_split.size(); i++) {
				worker_list.push_back(std::string(value_split[i].c_str()));
				DEBUG("added:%s to worker list!\n",
						value_split[i].c_str());
			}
		}
		if (line.find("input_files") == 0) {
			REQ_FIELDS--;
			value_split = split(substr, ',');
			for (int i = 0; i < (int)value_split.size(); i++) {
				in_files.push_back(std::string(value_split[i].c_str()));
				DEBUG("added:%s to input file list!\n",
						value_split[i].c_str());
			}
		}
	}

	cfg_file.close();

	if (REQ_FIELDS != 0) {
		fprintf(stderr, "Incomplete config.ini file! (%s)\n",
				config_filename.c_str());
		return false;
	}

	mr_spec.num_workers = nworkers;
	mr_spec.num_out_files = noutfiles;
	mr_spec.map_kb = map_kb;
	mr_spec.map_bytes = (map_kb * 1024);
	mr_spec.userid = userid;
	mr_spec.output_dir = output_dir;

	mr_spec.worker_list = worker_list;
	mr_spec.in_files = in_files;

	return true;
}

/*
 * CS6210_TASK: validate the specification read from the config file
 */
inline bool
validate_mr_spec(const MapReduceSpec& mr_spec)
{
	std::string						str;

	/* Limit of 16 workers */
	if (mr_spec.num_workers < 1 || mr_spec.num_workers > 16) {
		fprintf(stderr, "invalid number of workers! (%d)\n",
				mr_spec.num_workers);
		return (false);
	}
	/* Limit of 16 output files */
	if (mr_spec.num_out_files < 1 || mr_spec.num_out_files > 16) {
		fprintf(stderr, "invalid number of output files! (%d)\n",
				mr_spec.num_out_files);
		return (false);
	}

	if (mr_spec.map_kb < 100 || mr_spec.map_kb > 16384) {
		/* 100KB - 16MB limit */
		fprintf(stderr, "invalid shard size! (%d)\n", mr_spec.map_kb);
		return (false);
	}

	/*
	 * validate worker_list
	 * XXX: not validating if the endpoint exists or not
	 */
	if ((int)mr_spec.worker_list.size() != mr_spec.num_workers) {
		fprintf(stderr, "workerList and nWorker values differ! (%d:%d)\n",
				(int)mr_spec.worker_list.size(), mr_spec.num_workers);
		return (false);
	}

	/*
	 * validate input file list
	 * XXX: only checking localFS accessibility
	 */
	FILE *fp = NULL;
	for (int i = 0; i < (int)mr_spec.in_files.size(); i++) {
		fp = NULL;
		if (fp = fopen(mr_spec.in_files[i].c_str(), "r")) {
			fclose(fp);
		} else {
			fprintf(stderr, "file not accessible! (%s)\n", mr_spec.in_files[i].c_str());
			return (false); /* file not reachable! */
		}
	}

	return (true);
}
