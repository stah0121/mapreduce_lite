#pragma once

#include <mutex>
#include <thread>
#include <grpc++/grpc++.h>
#include <grpc/support/log.h>
#include "masterworker.grpc.pb.h"

#include "mapreduce_spec.h"
#include "file_shard.h"

#define DEBUG(_fmt, ...)

#define DEBUG2(_fmt, ...)	{				\
	printf("%s:%d ", __FILE__, __LINE__); 	\
	printf((_fmt), ##__VA_ARGS__);			\
}

enum ClientState { IDLE, BUSY, FAULTY };

class ReduceTask {
	public:
	int							partition_num;
	std::string					userid;
	std::vector<std::string>	files;
	int							state;
	void						*inprog_tag;
	std::string					client;
};

class WorkerClient {
	public:
	WorkerClient(std::shared_ptr<grpc::Channel> channel, std::string& ipaddr);

	void MapReduceWork(masterworker::WorkReq);
	void AsyncComplete(void *, void *, int);

	struct AsyncClientCall {
		masterworker::WorkReply		reply;
		grpc::ClientContext			context;
		grpc::Status				status;

		std::unique_ptr<grpc::ClientAsyncResponseReader<masterworker::WorkReply>> rsp_reader;
	};

	void												*inprog_tag;
	std::mutex											mtx;
	std::unique_ptr<masterworker::MasterWorker::Stub>	stub;
	grpc::CompletionQueue								cq;
	ClientState											state;
	std::string											ip_port;
};

/*
 * CS6210_TASK: Handle all the bookkeeping that Master is supposed to do.
 * This is probably the biggest task for this project, will test your
 * understanding of map reduce
 */
class Master {

	public:
	/* DON'T change the function signature of this constructor */
	Master(const MapReduceSpec&, const std::vector<FileShard>&);

	/* DON'T change this function's signature */
	bool run();

	/*
	 * NOW you can add below, data members and member functions as per
	 * the need of your implementation
	 */
	void RunMapJob();
	void RunReduceJob();

	FileShard * get_next_map_task();
	ReduceTask * get_next_reduce_task();
	
	//FileShard * get_map_work();

	void save_intermediate(const char *);
	void rt_add_interm(int pnum, std::string& file);
	void update_task(const std::string& type, void *tag, int state);
	int mapreduce_complete(const std::string& type);

	private:
	std::mutex						mtx;
	std::vector<FileShard>			map_tasks;
	std::vector<ReduceTask *>		reduce_tasks;
	std::vector<WorkerClient *>		clients;
	std::vector<std::string>		m_interm_files;

	std::vector<FileShard>			shards;
	MapReduceSpec					mr_config;
};

/* ReduceTask Add Intermediate File */
inline void Master::rt_add_interm(int pnum, std::string& file)
{
	DEBUG("adding file:%s to partition:%d\n", file.c_str(), pnum);

	for (int i = 0; i < (int)reduce_tasks.size(); i++) {
		if (reduce_tasks[i]->partition_num == pnum) {
			DEBUG("partition:%d found for rt_add_interm\n", pnum);
			reduce_tasks[i]->files.push_back(std::string(file));
			return;
		}
	}
}
