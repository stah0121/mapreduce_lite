#pragma once

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <math.h>
#include <fstream>
#include "mapreduce_spec.h"

#define DEBUG(_fmt, ...)

#define DEBUG2(_fmt, ...)	{				\
	printf("%s:%d ", __FILE__, __LINE__); 	\
	printf((_fmt), ##__VA_ARGS__);			\
}

enum JobState { INIT = 1, INPROG, DONE };

/* structure to store per input-file information for a particular FileShard */
struct ShardEntry {
	int				init;		/* mark initialization of this entry */
	std::string		in_file;	/* input file this entry represents */
	int				start;		/* starting line number */
	int				end;		/* ending line number */
};

/*
 * CS6210_TASK: Create your own data structure here, where you can hold information
 * about file splits, that your master would use for its own bookkeeping and to convey
 * the tasks to the workers for mapping
 */
struct FileShard {
	std::vector<ShardEntry> entries;		/* vector of ShardEntry objects */
	int						size;			/* size of shard in bytes */
	int						state;			/* <JobState> state */
	int						client_num;		/* current client number serving this task */
	void					*inprog_tag;	/* tag for this task while INPROG */
};

/*
 * CS6210_TASK: Create fileshards from the list of input files, map_kilobytes etc.
 * using mr_spec you populated
 */
inline bool
shard_files(const MapReduceSpec& mr_spec, std::vector<FileShard>& fileShards)
{
	double						input_bytes = 0;
	int							input_size = 0;
	FILE						*fp = NULL;
	FileShard					shard;
	ShardEntry					entry;
	int							M = 0;
	ssize_t						nread = 0;
	char						*line = NULL;
	size_t						len = 0;
	int							cur_file_offset = 0;

	/*
	 * (2) get M=ceil(tot_size / shard_size)
	 * (3) identify start & end positions within the 1st file and last file
	 * 		for a particular shard and append it to fileShards vector
	 */

	 /* get total size of all files combined in KB */
	for (int i = 0; i < (int)mr_spec.in_files.size(); i++) {
		fp = NULL;
		fp = fopen(mr_spec.in_files[i].c_str(), "r");
		if (!fp) {
			fprintf(stderr, "fp NULL - file:%s\n", mr_spec.in_files[i].c_str());
			return (false);
		}
		fseek(fp, 0L, SEEK_END);
		input_bytes += ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		fclose(fp);
	}

	/* convert B -> KB and round up to ensure we have enough shards */
	//DEBUG("input_bytes:%fB => ", input_bytes);
	input_bytes = ceil(input_bytes / 1024.00);
	//printf("input_size:%fKB\n", input_bytes);

	/* set number of shards M */
	M = (int)ceil(input_bytes / ((double) mr_spec.map_kb));	
	DEBUG("FileShard count M:%d\n", M);

	/*
	 * if M == 0, then we know map_kb > input_bytes, so set M to 1 so
	 * we can ensure small input sizes are properly handled
	 */
	if (M == 0)
		M = 1;

	/* start fresh FileShard and ShardEntry */
	shard.entries.clear();
	shard.size = 0;
	entry.in_file.clear();
	entry.init = entry.start = entry.end = 0;

	/* loop on all input files */
	for (int i = 0; i < (int)mr_spec.in_files.size(); i++) {

		DEBUG("creating shards from input_file:%s\n", mr_spec.in_files[i].c_str());

		/* open current file */
		fp = NULL;
		fp = fopen(mr_spec.in_files[i].c_str(), "r");
		if (!fp) {
			fprintf(stderr, "error opening file:%s\n", mr_spec.in_files[i].c_str());
			return (false);
		}
		/* initialize our offset */
		cur_file_offset = 0;

		/*
		 * our file is opened, now we start tracking shard boundaries
		 */
		while ((nread = getline(&line, &len, fp)) != -1) {

			/* check if this is a new ShardEntry */
			if (entry.init == 0) {
				entry.in_file = mr_spec.in_files[i];
				entry.init = 1;
				if (cur_file_offset == 0)
					entry.start = cur_file_offset;
				else
					entry.start = cur_file_offset+1;

				DEBUG("init ShardEntry file:%s start:%d\n",
						entry.in_file.c_str(), entry.start);
			}

			shard.size += (int)len;
			cur_file_offset++;

			/* check if shard is complete */
			if (shard.size >= mr_spec.map_bytes) {

				/* finalize and append last ShardEntry */
				entry.end = cur_file_offset;
				shard.entries.push_back(entry);

				/* insert shard into vector */
				fileShards.push_back(shard);

				DEBUG2("FileShard COMPLETE! start:%d end:%d size:%d map_bytes:%d\n",
						entry.start, entry.end, shard.size, mr_spec.map_bytes);

				/* reset shard entry */
				entry.in_file.clear();
				entry.init = entry.start = entry.end = 0;

				/* clear relevant FileShard info */
				shard.entries.clear();
				shard.size = 0;
			}

			/* cleanup */
			if (line) free(line);
			len = 0;
			line = NULL;
		}

		/* check if the file ended on us */
		if (entry.init == 1) {
			entry.end = cur_file_offset;
			/* account for final getline call as well (might be 0 but never negative) */
			shard.size += (int)len;

			/* close out ShardEntry since file ended */
			shard.entries.push_back(entry);
			DEBUG2("closing out ShardEntry! start:%d end:%d in_file:%s\n",
					entry.start, entry.end, entry.in_file.c_str());
			entry.in_file.clear();
			entry.init = entry.start = entry.end = 0;

			/* if final data to be read, or we hit our target size */
			if ((((int)mr_spec.in_files.size()-1) == i) ||
					(shard.size >= mr_spec.map_bytes)) {
				fileShards.push_back(shard);
				DEBUG2("FileShard COMPLETE! start:%d end:%d size:%d map_bytes:%d\n",
						entry.start, entry.end, shard.size, mr_spec.map_bytes);

				/* clear relevant FileShard info */
				shard.entries.clear();
				shard.size = 0;
			}
		}

		fclose(fp);
		fp = NULL;

	} //end foreach in_file

	DEBUG2("FileShards created:%d\n", (int)fileShards.size());

	return (true);
}
